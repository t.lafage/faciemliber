import { Component, OnInit, Input } from '@angular/core';

import { User } from '../../../models/user';
import { UserService } from 'src/app/services/user.service';
import { FriendService } from 'src/app/services/friend.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

    @Input() user: User;

    public friends: User[] = [];

    public isMyFriend: boolean;

    constructor(private friendService: FriendService) { }

    public ngOnInit() {

        this.getMyFriends();
        this.setIsUserIsMyFriend();

    }

    public getMyFriends(): void {

        if (this.friendService.myFriends.length)
            this.friends = this.friendService.myFriends;

    }

    public setIsUserIsMyFriend(): void {

        console.log(this.friends);
        console.log(this.user);

        console.log(this.friends.indexOf(this.user));

        for (let friend of this.friends) {

            if (friend.name == this.user.name) {

                this.isMyFriend = true;
                return;

            }
        }


    }

}
