import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../../services/auth.service';

import { User } from '../../../../models/user';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent{
    public email = this.email;
    public password = this.password;

    public user: User = new User( '', '');

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    login(): void {

        this.user.email = this.email;
        this.user.password = this.password;

        this.authService.auth(this.user)
            .subscribe( _ => this.router.navigateByUrl('/'));

    }
}
