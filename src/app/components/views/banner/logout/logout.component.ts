import { Component } from '@angular/core';
import { AuthService } from '../../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.scss']
})

export class LogoutComponent {

    constructor(
        public authService: AuthService,
        private router: Router,
    ) {

        this.router.routeReuseStrategy.shouldReuseRoute = _ => false;

    }

    public logout(): void {

        this.authService.user = null;
        this.router.navigateByUrl('/home');

    }

    public navigateToProfil(): void {

        this.router.navigateByUrl('/' + this.authService.user.pseudo);

    }

}
