import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserService } from '../../../../services/user.service';
import { AuthService } from '../../../../services/auth.service';
import { FriendService } from '../../../../services/friend.service';

import { User } from '../../../../models/user';

@Component({
    selector: 'app-profil',
    templateUrl: './profil.component.html',
    styleUrls: ['./profil.component.scss']
})

export class ProfilComponent implements OnInit {

    public user: User;

    public isMyWall = false;
    public isMyFriend = false;

    constructor(
        private route: ActivatedRoute,
        private userService: UserService,
        public friendService: FriendService,
        public authService: AuthService
    ) { }

    public ngOnInit(): void {

        this.getUser();

    }

    public getUser(): void {

        if (this.authService.user.pseudo == this.route.snapshot.paramMap.get('pseudo')) {

            this.user = this.authService.user;
            this.isMyWall = true;

            this.setIsMyFriend();
            this.setProfilfriends();

        }

        else this.userService.findWithPseudo(this.route.snapshot.paramMap.get('pseudo'))
            .subscribe(user => {

                this.user = user

                this.setIsMyFriend();
                this.setProfilfriends();

            });

    }

    public setProfilfriends(): void {

        this.friendService.profilFriends = [];

        this.friendService.setProfilFriends(this.user.id);

    }

    public setIsMyFriend(): void {

        if (this.friendService.myFriends.length) {

            for (let friend of this.friendService.myFriends) {

                if (friend.id == this.user.id) {

                    console.log('c\'est mon ami !');
                    this.isMyFriend = true;
                    return;

                }

            }
        }

    }

    public addFriend(): void {

        this.userService.addFriend(this.user.id)
            .subscribe(res => {
                console.log(res);
                this.isMyFriend = true;
            });

    }

}
