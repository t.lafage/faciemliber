import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { PostService } from '../../../../../services/post.service';
import { UserService } from '../../../../../services/user.service';
import { AuthService } from 'src/app/services/auth.service';

import { Post } from '../../../../../models/post';
import { User } from '../../../../../models/user';

@Component({
    selector: 'app-wall',
    templateUrl: './wall.component.html',
    styleUrls: ['./wall.component.scss']
})

export class WallComponent implements OnInit {

    public user: User;
    public posts: Post[] = [];

    public isMyWall = false

    public showSpinner = true;

    constructor(
        private postServcie: PostService,
        private userServcie: UserService,
        private route: ActivatedRoute,
        public authService: AuthService
    ) { }

    ngOnInit() {

        this.getUser();

    }

    getUser() {

        if (this.authService.user.pseudo == this.route.parent.snapshot.paramMap.get('pseudo')) {

            this.user = this.authService.user;
            this.isMyWall = true;
            this.getPosts();

        }

        else {

            this.userServcie.findWithPseudo(this.route.parent.snapshot.paramMap.get('pseudo'))
                .subscribe(user => {

                    this.user = user;
                    this.getPosts();

                });

        }

    }

    getPosts(): void {

        this.postServcie.allForAUser(this.user.id)
            .subscribe(posts => {

                this.posts = posts
                this.showSpinner = false;

            });

    }

}
