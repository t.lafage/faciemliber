import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

    public user: User;

    public showSpinner: boolean = true;

    constructor(
        private userService: UserService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {

        this.getUser();

    }

    getUser() {

        let user_pseudo = this.route.parent.snapshot.paramMap.get('pseudo');

        this.userService.findWithPseudo(user_pseudo)
            .subscribe(user => {

                this.user = user;
                this.showSpinner = false;

            });

    }

}
