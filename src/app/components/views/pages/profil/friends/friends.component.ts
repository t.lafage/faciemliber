import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { UserService } from 'src/app/services/user.service';
import { FriendService } from 'src/app/services/friend.service';

import { User } from 'src/app/models/user';

@Component({
    selector: 'app-friends',
    templateUrl: './friends.component.html',
    styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {

    public user: User;

    // public showSpinner: boolean = true;

    constructor(
        private userService: UserService,
        public friendService: FriendService,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {

        this.getUser();

    }

    getUser() {

        let user_pseudo = this.route.parent.snapshot.paramMap.get('pseudo');

        this.userService.findWithPseudo(user_pseudo)
            .subscribe(user => {

                this.user = user;
                // this.getFriends();

            });

    }

    getFriends() {

        if (!this.friendService.profilFriends.length)
            this.friendService.setProfilFriends(this.user.id);

        // this.userService.findFriends(this.user.id)
        //     .subscribe(l_friends => {

        //         for (let l_friend of l_friends)
        //             this.friends.push(l_friend.friend);

        //     });

    }

}
