import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth.service';

import { User } from 'src/app/models/user';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    public firstname: string;
    public lastname: string;
    public pseudo: string;
    public anniversary: Date;
    public password: string;
    public checkPassword: string;
    public email: string;

    public name: string;
    public user: User;

    public error = '';

    public invalidCheckPassword = false;

    public emptyFirstName = false;
    public emptyLastName = false;
    public emptyPseudo = false;
    public emptyAnniversary = false;
    public emptyPassword = false;
    public emptyEmail = false;

    constructor(
        private authService: AuthService,
        private router: Router
    ) { }

    ngOnInit(): void {

        // this.email = this.user.email;
        // this.password = this.user.password;

    }

    capitalize(str): string {

        return str.charAt(0).toUpperCase() + str.slice(1);

    }

    checkPasswordConfirm(): boolean {

        return this.invalidCheckPassword = (this.password !== this.checkPassword) ? true : false;
    }

    checkInvalidFiels(): void {

        if (!this.firstname) {

            this.error = 'Veuillez mettre un prénom';
            this.emptyFirstName = true;

        }

        else this.emptyFirstName = false;

        if (!this.lastname) {

            this.error = 'Veuillez mettre un nom';
            this.emptyLastName = true;

        }

        else this.emptyLastName = false;

        if (!this.pseudo) {

            this.error = 'Veuillez mettre un identifiant';
            this.emptyPseudo = true;

        }

        else this.emptyPseudo = false;

        if (!this.anniversary) {

            this.error = 'Veuillez mettre une date de naissance';
            this.emptyAnniversary = true;

        }

        else this.emptyAnniversary = false;

        if (!this.password) {

            this.error = 'Veuillez mettre un mot de passe';
            this.emptyPassword = true;

        }

        else this.emptyPassword = false;

        if (!this.email) {

            this.error = 'Veuillez mettre un e-mail';
            this.emptyEmail = true;

        }

        else this.emptyEmail = false;

    }

    register(): void {


        if (this.checkPasswordConfirm()) return;
        if (this.emptyFirstName == true ||
            this.emptyLastName == true ||
                this.emptyPseudo == true ||
                    this.emptyAnniversary == true ||
                        this.emptyPassword == true ||
                            this.emptyEmail == true ) return;

        this.user = new User(this.email, this.password);

        this.user.name = this.capitalize(this.firstname) + ' ' + this.capitalize(this.lastname);
        this.user.pseudo = this.pseudo;
        this.user.anniversary = this.anniversary;

        console.log(this.user);

        this.authService.register(this.user)
            .subscribe((user) => this.router.navigateByUrl('/'));




        // if( this.password !== this.checkPassword ) {
        //     this.invalid = true

        // }

        // this.user = new User( this.email, this.password);

        // this.name = this.capitalize(this.firstname) + ' ' + this.capitalize(this.lastname)

        // // console.log(this.name);

        // // this.user = new User( this.email, this.password);

        // this.user.name = this.name;
        // this.user.anniversary = this.anniversary;

        // console.log(this.user);

        // if( this.email)


    }

}
