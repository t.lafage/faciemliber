import { Component, OnInit } from '@angular/core';

import { Post } from '../../../../models/post';

import { PostService } from '../../../../services/post.service';
import { AuthService } from '../../../../services/auth.service';
import { FriendService } from '../../../../services/friend.service';

@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})

export class NewsComponent implements OnInit {

    public posts: Post[] = [];

    constructor(
        private postServcie: PostService,
        private friendService: FriendService,
        public authService: AuthService
    ) { }

    ngOnInit() {

        if( this.authService.user ) {

            this.getPosts();
            this.friendService.setMyFriends(this.authService.user.id);
        }

    }

    getPosts(): void {

        this.postServcie.allFriendPost( this.authService.user.id)
            .subscribe(a_posts => {

                for (let posts of a_posts)
                    for(let post of posts)
                        this.posts.push(post)


            });

    }

}
