import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CommentService } from 'src/app/services/comment.service';
import { UserService } from 'src/app/services/user.service';

import { User } from 'src/app/models/user';
import { Post } from 'src/app/models/post';
import { Comment } from 'src/app/models/comment';
import { PostService } from 'src/app/services/post.service';

@Component({
    selector: 'app-post-info',
    templateUrl: './post-info.component.html',
    styleUrls: ['./post-info.component.scss']
})

export class PostInfoComponent implements OnInit {

    @Input() post: Post;

    public likes = [];

    public comments: Comment[] = [];

    public display_like = false;
    public showSpinner = false;

    constructor(
        private commentServcie: CommentService,
        private postServcie: PostService
    ) { }


    ngOnInit() {
        // console.log(this.post);

    }

    dpComments() {

        if(!this.comments.length ) {

            this.showSpinner = true;
            this.getComments();

        }

        else this.comments = [];
    }

    dpLikes() {

        if(!this.display_like ) {

            this.display_like = true;

        }

        else this.display_like = false;
    }


    getComments(): void {

        this.commentServcie.allForAPost(this.post.id)
            .subscribe(comments => {
                this.comments = comments;
                this.showSpinner = false;
            });
//  this.comments = comments)
    }

    addLike(): void {

        this.postServcie.addLike(this.post.id).subscribe();

    }



}
