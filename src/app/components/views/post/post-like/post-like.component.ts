import { Component, OnInit, Input, AfterViewInit, EventEmitter, Output } from '@angular/core';

import { UserService } from 'src/app/services/user.service';

import { User } from 'src/app/models/user';
import { Post } from 'src/app/models/post';

@Component({
    selector: 'app-post-like',
    templateUrl: './post-like.component.html',
    styleUrls: ['./post-like.component.scss']
})
export class PostLikeComponent implements OnInit {

    @Input() post: Post;

    @Output() display_like = new EventEmitter();

    public likes: User[] = [];

    public showSpinner: boolean = true;

    constructor(private userServcie: UserService) { }

    ngOnInit() {

        this.getLikes();

    }

    close() {

        this.display_like.emit()
    }

    getLikes(): void {

        this.userServcie.allLikes(this.post.id)
            .subscribe(users => {

                this.likes = users;
                this.showSpinner = false;

            });
    }

}
