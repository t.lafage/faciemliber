import { Component} from '@angular/core';

import { PostService } from '../../../../services/post.service';
import { Post } from '../../../../models/post';

@Component({
    selector: 'app-response',
    templateUrl: './response.component.html',
    styleUrls: ['./response.component.scss']
})
export class ResponseComponent {

    public response: string;

    constructor(private postService: PostService) { }

    public sendResponse(): void {

        if(this.response) {

            this.postService.send({ content: this.response } as Post)
                .subscribe( res => console.log(res));

            this.response = undefined;

        }

    }

}
