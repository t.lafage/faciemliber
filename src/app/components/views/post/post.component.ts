import { Component, OnInit, Input, Output } from '@angular/core';

import { CommentService } from '../../../services/comment.service';

import { Post } from '../../../models/post';
import { Comment } from '../../../models/comment';
import { UserService } from '../../../services/user.service';
import { User } from '../../../models/user';
// import './assets';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.scss']
})

export class PostComponent implements OnInit {

    @Input() post: Post;



    public showSpinner: boolean = false;

    constructor() { }

    ngOnInit() {
        // console.log(this.post);

    }

}
