import { Component, Input } from '@angular/core';

import { CommentService } from 'src/app/services/comment.service';

import { Post } from 'src/app/models/post';
import { Comment } from 'src/app/models/comment';

@Component({
    selector: 'app-new-comment',
    templateUrl: './new-comment.component.html',
    styleUrls: ['./new-comment.component.scss']
})
export class NewCommentComponent {

    @Input() post: Post;

    public newComment: string;

    constructor(private commentService: CommentService) { }

    public sendNewComment(): void {

        if(this.newComment) {

            this.commentService.send(
                    { content: this.newComment } as Comment,
                    this.post.id
                )
                .subscribe( res => {

                    console.log(res);


                });

            this.newComment = undefined;

        }

    }

}
