import { Model } from './model';
import { User } from './user';

export class Post extends Model {

    public content: string;
    public date_creation: Date;
    public author_id: number;

    public author?: User;
    public comments_count?: number;
    public likes_count?: number;
    public nb_like?: number;

}
