import { Model } from './model';
import { User } from './user';

export class Comment extends Model {

    public content: string;
    public date_creation: Date;
    public author_id: number;
    public post_id: number;

    public author?: User;

}
