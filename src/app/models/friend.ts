import { User } from './user';

export interface FriendLiteral {

    user_id: number;
    friend_id: number;
    friend: User;

}
