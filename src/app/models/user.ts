import { Model } from './model';

export class User extends Model {

    public name: string;
    public pseudo: string;
    public email: string;
    public anniversary: Date;
    public image: string;

    public password?: string;

    constructor(email, password) {

        super();

        this.email = email;
        this.password = password;

    }

}
