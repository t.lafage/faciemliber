import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthService } from './services/auth.service';

import { HomeComponent } from './components/views/pages/home/home.component';

import { MainComponent } from './components/views/main/main.component';

import { NewsComponent } from './components/views/pages/news/news.component';
import { UserListComponent } from './components/views/pages/user-list/user-list.component';
import { ProfilComponent } from './components/views/pages/profil/profil.component';

import { WallComponent } from './components/views/pages/profil/wall/wall.component';
import { AboutComponent } from './components/views/pages/profil/about/about.component';
import { FriendsComponent } from './components/views/pages/profil/friends/friends.component';


const routes: Routes = [
    { path: 'home', component: HomeComponent },
];

const privateRoutes: Routes = [
    {
        path: '',
        component: MainComponent,
        canActivate: [ AuthService ],
        children: [
            { path: '', redirectTo: 'news', pathMatch: 'full' },
            { path: 'news', component: NewsComponent },
            { path: 'user-list', component: UserListComponent },
            {
                path: ':pseudo',
                component: ProfilComponent,
                children: [
                    { path: '', redirectTo: 'wall', pathMatch: 'full' },
                    { path: 'wall', component: WallComponent },
                    { path: 'about', component: AboutComponent },
                    { path: 'friends', component: FriendsComponent }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
        RouterModule.forChild(privateRoutes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }
