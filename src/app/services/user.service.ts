import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

import { AuthService } from './auth.service';

import { User } from '../models/user';
import { FriendLiteral } from '../models/friend';

const serviceUrl: string = 'http://localhost:8000/api/users';

@Injectable({
    providedIn: 'root'
})

export class UserService {

    constructor(
        private http: HttpClient,
        private authService: AuthService
    ) { }

    all(): Observable<User[]> {

        return this.http.get<User[]>(serviceUrl);

    }

    find(id: number): Observable<User> {

        return this.http.get<User>(serviceUrl + '/' + id);

    }

    allLikes(post_id: number): Observable<User[]> {

        return this.http.get<User[]>(serviceUrl + '/post/' + post_id);

    }

    findWithPseudo(pseudo: string): Observable<User> {

        return this.http.get<User>(serviceUrl + '/pseudo/' + pseudo);

    }

    addFriend(friend_id: number): Observable<User> {

        const formData: FormData = new FormData();

        formData.append('user_id', this.authService.user.id.toString());
        formData.append('friend_id', friend_id.toString());

        console.log('rr');


        return this.http.post<User>(serviceUrl + '/add/friend', formData);

        // return this.http.get<User>(serviceUrl + '/pseudo/' + pseudo);

    }

    findFriends(user_id: number): Observable<FriendLiteral[]> {

        return this.http.get<FriendLiteral[]>(`${serviceUrl}/${user_id}/friends`);

    }
}
