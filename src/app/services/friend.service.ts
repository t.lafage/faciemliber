import { Injectable } from '@angular/core';

import { UserService } from './user.service';

import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
})

export class FriendService {

    public myFriends: User[] = [];
    public profilFriends: User[] = [];

    public notProfilFriends = false;

    constructor(private userService: UserService) { }

    setMyFriends(user_id: number): void {

        this.userService.findFriends(user_id)
            .subscribe(l_friends => {

                for (let l_friend of l_friends)
                    this.myFriends.push(l_friend.friend);

            });

    }

    setProfilFriends(user_id: number): void {

        this.userService.findFriends(user_id)
            .subscribe(l_friends => {

                this.notProfilFriends = (!l_friends.length) ? true : false;

                this.profilFriends = [];

                for (let l_friend of l_friends)
                    this.profilFriends.push(l_friend.friend);

            });

    }
}
