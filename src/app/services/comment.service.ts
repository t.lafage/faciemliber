import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { UserService } from './user.service';
import { Comment } from '../models/comment';
import { AuthService } from './auth.service';

const serviceUrl: string = 'http://localhost:8000/api/comments';

@Injectable({
    providedIn: 'root'
})

export class CommentService {

    constructor(
        private http: HttpClient,
        private userService: UserService,
        private authService: AuthService
    ) { }

    allForAPost(id: number): Observable<Comment[]> {

        return this.http.get<Comment[]>(serviceUrl + '/post/' + id);
        // .pipe(
        //     tap(posts => {

        //         for (let post of posts) {

        //             this.userService.find(post.author_id)
        //                 .subscribe(user => post.user = user);

        //         }

        //         return posts

        //     }));

    }

    send(comment: Comment, post_id: number): Observable<Comment> {

        const formData: FormData = new FormData();

        // TODO insertion de commentaire dans l'api
        formData.append('content', comment.content);
        formData.append('author_id', this.authService.user.id.toString() );
        formData.append('post_id', post_id.toString() );

        return this.http.post<Comment>(serviceUrl, formData);

    }
}
