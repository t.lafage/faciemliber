import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { tap } from "rxjs/operators";

import { Observable } from 'rxjs';

import { Post } from '../models/post';
import { AuthService } from './auth.service';

const serviceUrl: string = 'http://localhost:8000/api/posts';

@Injectable({
    providedIn: 'root'
})

export class PostService {

    constructor(
        private http: HttpClient,
        private authService: AuthService
    ) { }

    // all(): Observable<Post[]> {

    //     return this.http.get<Post[]>(serviceUrl);

    // }

    allForAUser(id: number): Observable<Post[]> {

        return this.http.get<Post[]>(`${serviceUrl}/${id}/wall`);

    }

    allFriendPost(id: number): Observable<Array<Post[]>>  {

        return this.http.get<Array<Post[]>>(`${serviceUrl}/${id}/news`);

    }

    send(post: Post): Observable<Post> {

        const formData: FormData = new FormData();

        formData.append('content', post.content);
        formData.append('author_id', this.authService.user.id.toString() );

        return this.http.post<Post>(serviceUrl, formData);

    }

    addLike(post_id: number): Observable<any> {

        const formData: FormData = new FormData();

        formData.append('user_id', this.authService.user.id.toString());
        formData.append('post_id', post_id.toString());

        return this.http.post(serviceUrl + '/add/like', formData);

    }

}
