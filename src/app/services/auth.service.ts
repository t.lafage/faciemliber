import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs';

import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

const serviceUrl: string = 'http://localhost:8000/api/users/auth';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    public user: User = null;

    constructor(
        private http: HttpClient,
        private router: Router
    ) { }

    public auth(user: User): Observable<User> {


        const formData: FormData = new FormData();

        formData.append('email', user.email);
        formData.append('password', user.password);

        console.log(user.password);

        return this.http.post<User>(serviceUrl, formData)
            .pipe(
                tap(user => {

                    console.log(user);

                    if (!user.name) return;
                    this.user = user;

                })
            );

    }

    public register(user: User): Observable<User> {

        const formData: FormData = new FormData;

        formData.append('name', user.name);
        formData.append('email', user.email);
        formData.append('password', user.password);
        formData.append('pseudo', user.pseudo);
        formData.append('anniversary', user.anniversary.toString());
        formData.append('image', '5.jpeg');

        return this.http.post<User>('http://localhost:8000/api/users/register', formData)
            .pipe(
                tap(user => {

                    if (!user.name) return;
                    this.user = user;

                })
            );
    }

    canActivate(): boolean {

        if (this.user !== null) return true;

        this.router.navigateByUrl('/home');

        return false;
    }
}
