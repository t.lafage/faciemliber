import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';

registerLocaleData(localeFr, 'fr', localeFrExtra);

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BannerComponent } from './components/views/banner/banner.component';
import { LoginComponent } from './components/views/banner/login/login.component';
import { CommentComponent } from './components/views/post/comment/comment.component';
import { NewsComponent } from './components/views/pages/news/news.component';
import { MainComponent } from './components/views/main/main.component';
import { LogoutComponent } from './components/views/banner/logout/logout.component';
import { HomeComponent } from './components/views/pages/home/home.component';

import { ProfilComponent } from './components/views/pages/profil/profil.component';
import { FriendsComponent } from './components/views/pages/profil/friends/friends.component';
import { WallComponent } from './components/views/pages/profil/wall/wall.component';
import { AboutComponent } from './components/views/pages/profil/about/about.component';

import { PostComponent } from './components/views/post/post.component';
import { PostInfoComponent } from './components/views/post/post-info/post-info.component';
import { PostLikeComponent } from './components/views/post/post-like/post-like.component';
import { RegisterComponent } from './components/views/pages/home/register/register.component';
import { ResponseComponent } from './components/views/post/response/response.component';
import { SearchComponent } from './components/views/banner/logout/search/search.component';

import { LoaderComponent } from './components/tools/loader/loader.component';

import { UserComponent } from './components/views/user/user.component';
import { UserListComponent } from './components/views/pages/user-list/user-list.component';
import { FirstWordPipe } from './pipes/first-word.pipe';
import { NewCommentComponent } from './components/views/post/new-comment/new-comment.component';

@NgModule({
    declarations: [
        AppComponent,
        BannerComponent,
        LoginComponent,
        RegisterComponent,
        CommentComponent,
        UserListComponent,
        UserComponent,
        NewsComponent,
        SearchComponent,
        PostInfoComponent,
        PostComponent,
        LoaderComponent,
        MainComponent,
        LogoutComponent,
        ResponseComponent,
        ProfilComponent,
        HomeComponent,
        FriendsComponent,
        WallComponent,
        AboutComponent,
        FriendsComponent,
        PostLikeComponent,
        FirstWordPipe,
        NewCommentComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    providers: [
        { provide: LOCALE_ID, useValue: "fr-FR" },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
